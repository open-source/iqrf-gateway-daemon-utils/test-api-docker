#!/bin/bash

echo "Sleeping 5s ..."
sleep 5

echo "Romoving logs ..."
rm -f /home/iqrf/iqrftech/test-api-app/test-api-app/log/*.log
rm -f /home/iqrf/iqrftech/test-api-app/test-api-app/log/*.json

echo "Running test ..."
mono test-api-app.exe
